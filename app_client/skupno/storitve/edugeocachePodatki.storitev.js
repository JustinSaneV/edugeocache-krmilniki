(function() {
  /* global angular */
  
  var edugeocachePodatki = function($http) {
    var koordinateTrenutneLokacije = function(lat, lng) {
      return $http.get('/api/lokacije?lng=' + lng + '&lat=' + lat + '&maxRazdalja=100');
    };
    var podrobnostiLokacijeZaId = function(idLokacije) {
      return $http.get('/api/lokacije/' + idLokacije);
    };
    var dodajKomentarZaId = function(idLokacije, podatki) {
      return $http.post('/api/lokacije/' + idLokacije + '/komentarji', podatki);
    };
    return {
      koordinateTrenutneLokacije: koordinateTrenutneLokacije,
      podrobnostiLokacijeZaId: podrobnostiLokacijeZaId,
      dodajKomentarZaId: dodajKomentarZaId
    };
  };
  edugeocachePodatki.$inject = ['$http'];
  
  angular
    .module('edugeocache')
    .service('edugeocachePodatki', edugeocachePodatki);
})();