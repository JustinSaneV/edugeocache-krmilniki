(function() {
  /* global angular */
  
  var navigacija = function() {
    return {
      restrict: 'EA',
      templateUrl: "/skupno/direktive/navigacija/navigacija.predloga.html"
    };
  };
  
  angular
    .module('edugeocache')
    .directive('navigacija', navigacija);
})();