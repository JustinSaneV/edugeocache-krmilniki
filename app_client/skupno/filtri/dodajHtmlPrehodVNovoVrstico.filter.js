(function() {
  /* global angular */

  var dodajHtmlPrehodVNovoVrstico = function() {
    return function(besedilo) {
      var rezultat = besedilo.replace(/\n/g, '<br/>');
      return rezultat;
    };
  };
  
  angular
    .module('edugeocache')
    .filter('dodajHtmlPrehodVNovoVrstico', dodajHtmlPrehodVNovoVrstico);
})();